# FibRust

This is rust-backed version of fibonacci function.

## Performance comparison

```
❯ rake benchmark
Calculating -------------------------------------
                ruby    542.633k (± 0.4%) i/s -      2.721M in   5.014871s
      ruby_recursive      4.761  (± 0.0%) i/s -     24.000  in   5.041325s
    ruby_memoization    195.055k (± 1.2%) i/s -    989.456k in   5.073389s
            fib_rust      8.051M (± 0.5%) i/s -     40.276M in   5.002752s

Comparison:
            fib_rust:  8051043.9 i/s
                ruby:   542633.4 i/s - 14.84x  (± 0.00) slower
    ruby_memoization:   195055.3 i/s - 41.28x  (± 0.00) slower
      ruby_recursive:        4.8 i/s - 1691152.45x  (± 0.00) slower
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'fib_rust'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install fib_rust

## Usage

```ruby
2.6.6 :001 > FibRust[40]
 => 102334155
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/dgruzd/fib_rust.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
