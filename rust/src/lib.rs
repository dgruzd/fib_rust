#[no_mangle]
pub extern "C" fn fib(n: u32) -> u32 {
    let mut x = (0, 1);
    for _ in 0..n {
        x = (x.1, x.0 + x.1)
    }
    x.0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(fib(5), 5);
    }
}
