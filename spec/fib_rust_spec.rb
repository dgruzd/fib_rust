RSpec.describe FibRust do
  it "has a version number" do
    expect(FibRust::VERSION).not_to be nil
  end

  it "returns correct value" do
    expect(FibRust[5]).to eq(5)
  end
end
