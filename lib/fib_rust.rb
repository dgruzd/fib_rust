require 'fib_rust/version'
require 'fib_rust/ffi'

module FibRust
  def self.[](n)
    FibRust.fib(n)
  end
end
