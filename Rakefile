require "bundler/gem_tasks"
require "rspec/core/rake_task"

RSpec::Core::RakeTask.new(:spec)

task :default => :spec

task :rust_build do
  require 'ffi'
  `cd rust && cargo build --release`
  `mv -f ./rust/target/release/libfib.#{FFI::Platform::LIBSUFFIX} ./lib/fib_rust/`
end

task :rust_test do
  `cd rust && cargo test`
end

task :build => :rust_build
task :spec => :rust_build

task :test => %w[rust_test spec]

task :benchmark do
  require 'fib_rust'
  require 'benchmark/ips'

  def ruby_fib(iterations)
    first_num, second_num = [0, 1]

    iterations.times do
      first_num, second_num = second_num, first_num + second_num
    end

    first_num
  end

  def ruby_recursive(iterations)
    return iterations if (0..1).include? iterations

    (ruby_recursive(iterations - 1) + ruby_recursive(iterations - 2))
  end

  def ruby_memoization(iterations)
    fib = Hash.new do |numbers, index|
      numbers[index] = fib[index - 2] + fib[index - 1]
    end.update(0 => 0, 1 => 1)

    fib[iterations]
  end

  ITERATIONS = 30

  Benchmark.ips do |x|
    x.report('ruby') { ruby_fib(ITERATIONS) }
    x.report('ruby_recursive') { ruby_recursive(ITERATIONS) }
    x.report('ruby_memoization') { ruby_memoization(ITERATIONS) }
    x.report('fib_rust') { FibRust[ITERATIONS] }

    x.compare!
  end
end
task :bench => :benchmark
